
function Pokemon(name,level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;


	//Methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = (target.health - this.attack)
		if(target.health <= 0) {
			this.faint(target.name)
		}
	},

	this.faint = function(pokemon){
			console.log(pokemon + " fainted")}
}

let pikachu = new Pokemon ("Pikachu", 16);
let squirtle = new Pokemon ("Squirtle", 8);
console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)